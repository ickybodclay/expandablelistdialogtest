package com.detroitlabs.expandablelistdialogtest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.detroitlabs.expandablelistdialogtest.adapters.PairExpandableListAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFabClicked(view);
            }
        });
    }

    private AlertDialog dialog;

    private void onFabClicked(View view) {
        if (dialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            List<Pair<String, String>> pairs = new ArrayList<>();
            pairs.add(new Pair<>("A", "a"));
            pairs.add(new Pair<>("B", "b"));
            pairs.add(new Pair<>("C", "c"));
            pairs.add(new Pair<>("D", "d"));
            pairs.add(new Pair<>("E", "e"));

            View dialogContent = View.inflate(builder.getContext(), R.layout.dialog_content_expandable_list, null);
            ExpandableListView dialogExpandableListView = (ExpandableListView) dialogContent.findViewById(R.id.pairs_list);
            dialogExpandableListView.setAdapter(new PairExpandableListAdapter(builder.getContext(), pairs));
            dialogExpandableListView.setOnChildClickListener(this);

            // preselect first child
            dialogExpandableListView.expandGroup(0);
            dialogExpandableListView.setItemChecked(1, true);

            builder.setView(dialogContent);

            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    dialog = null;
                }
            });

            dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
        Log.d(MainActivity.class.getName(), String.format("onChildClick group=%d | child=%d | index=%d", groupPosition, childPosition, index));
        parent.setItemChecked(index, true);
        return true;
    }
}
