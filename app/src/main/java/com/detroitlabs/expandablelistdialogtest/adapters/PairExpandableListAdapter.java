package com.detroitlabs.expandablelistdialogtest.adapters;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import com.detroitlabs.expandablelistdialogtest.R;

import java.util.List;

public class PairExpandableListAdapter implements ExpandableListAdapter {

    private final LayoutInflater inflater;
    private final List<Pair<String, String>> pairs;
    private final DataSetObservable dataSetObservable = new DataSetObservable();

    public PairExpandableListAdapter(Context context, List<Pair<String, String>> pairs) {
        this.pairs = pairs;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        dataSetObservable.registerObserver(dataSetObserver);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        dataSetObservable.unregisterObserver(dataSetObserver);
    }

    @Override
    public int getGroupCount() {
        return pairs.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return pairs.get(i).first;
    }

    @Override
    public Object getChild(int i, int j) {
        return pairs.get(i).second;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int j) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_group_pair, parent, false);

            viewHolder = new GroupViewHolder();
            viewHolder.titleText = (TextView) convertView.findViewById(R.id.title);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupViewHolder) convertView.getTag();
        }

        String title = (String) getGroup(groupPosition);
        if (title != null) {
            viewHolder.titleText.setText(title);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ChildViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_child_pair, parent, false);

            viewHolder = new ChildViewHolder();
            viewHolder.descriptionText = (TextView) convertView.findViewById(R.id.description);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ChildViewHolder) convertView.getTag();
        }

        String description = (String) getChild(groupPosition, childPosition);
        if (description != null) {
            viewHolder.descriptionText.setText(description);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return pairs.isEmpty();
    }

    @Override
    public void onGroupExpanded(int i) {
    }

    @Override
    public void onGroupCollapsed(int i) {
    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return groupId;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return groupId;
    }

    private static class GroupViewHolder {
        TextView titleText;
    }

    private static class ChildViewHolder {
        TextView descriptionText;
    }

}
